KEYCLOAK_SERVER=
KEYCLOAK_REALM=
KEYCLOAK_CLIENT_NAME=
KEYCLOAK_CLIENT_SECRET=

# role admin used for app_qhub.py.  
# Use needs: `realm-management` roles: manage-clients, view-users
KEYCLOAK_ROLE_ADM=
KEYCLOAK_ROLE_ADM_PASSWD=

# user in keycloak for testing
# NOTE: set TEST_UID = "" - to allow actual header to be used in app_qhub.py
TEST_UID=
TEST_PASSWORD=