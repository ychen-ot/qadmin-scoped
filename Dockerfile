FROM condaforge/mambaforge:latest as base

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

ENV CONDA_ENV_NAME=qhub_qadmin_scope
ENV PATH /opt/conda/envs/$CONDA_ENV_NAME/bin:$PATH
ENV PYTHONUNBUFFERED=True

COPY environment.yml /tmp/envs/environment.yml

RUN apt update \
    && apt -y install locales \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8

RUN mamba env create -n $CONDA_ENV_NAME \
    --file /tmp/envs/environment.yml \
    && echo "source activate $CONDA_ENV_NAME" > ~/.bashrc \
    && conda clean --all \
    && find /opt/conda/ -type f,l -name '*.a' -delete \
    && find /opt/conda/ -type f,l -name '*.pyc' -delete \
    && find /opt/conda/ -type f,l -name '*.js.map' -delete \
    && rm -rf /opt/conda/pkgs

COPY . .

ENV PYTHONPATH='.'
EXPOSE 80

ENTRYPOINT bokeh serve app_qhub.py --port=80 --use-xheaders --allow-websocket-origin=$DEPLOY_HOST --prefix=$PREFIX
