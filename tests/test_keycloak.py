import json
import pytest

from utils.keycloak_admin_utils import KeycloakAdminClient

# sample keycloak client payload
TEST_REALM = "education"
TEST_CLIENT = "jakarta-school"
TEST_ROLE = "my_new_role"

# Keycloak configuration 
client = KeycloakAdminClient (
        server_url="http://localhost:8081/auth/realm",
        admin_user="admin",
        admin_pwd="admin",
        admin_realm="master",
        realm_name=TEST_REALM
    )


@pytest.fixture
def delete_test_role():
    res = client.delete_client_role(TEST_CLIENT, TEST_ROLE)
    print(f"role deleted - res: {res}")

def test_get_users():
    users = client.get_users()
    print("users: {}".format(json.dumps(users, indent=2)))
    assert users is not None


def test_get_client_roles():
    roles = client.get_client_roles(TEST_CLIENT)
    print("roles: {}".format(json.dumps(roles, indent=2)))
    assert roles is not None


def test_create_delete_client_roles(delete_test_role):
    role_name = TEST_ROLE
    res = client.create_client_role(TEST_CLIENT, role_name, "My role description")
    print(f"create role result: {res}")
    assert res == b''

    role = client.get_client_role(TEST_CLIENT, role_name)
    print(f"role retrieved: {role}")
    assert role["name"] == role_name

    res = client.delete_client_role(TEST_CLIENT, role_name)
    print(f"role deleted - res: {res}")
    assert res == {}

    # retrieve role again - should not be there
    # This will thro
    role = client.get_client_role(TEST_CLIENT, role_name)
    assert role == None
