import json
from permissions import get_qadmin_policy_for_user, RN_EMPLOYEE, RN_PROJECT_OVERVIEW, get_policy
from scoped_rbac.policy import Permission

import pytest

# sample keycloak client payload
DEFAULT_CONTEXT="qhub_qadmin"

payload_json = None

@pytest.fixture
def default_payload():
	global payload_json
	payload_json = {
		"exp": 1647384035,
		"iat": 1647382235,
		"jti": "40a78d13-a5f7-4ab5-aa23-ab85569b3831",
		"iss": "http://localhost:8081/auth/realms/education",
		"aud": "account",
		"sub": "d79db3ad-14cf-4466-9107-27e7de6363e8",
		"typ": "Bearer",
		"azp": "qhub_qadmin",
		"session_state": "4ce6f125-227d-4202-935b-e392ba061ebb",
		"acr": "1",
		"realm_access": {
			"roles": ["default-roles-education", "offline_access", "uma_authorization"]
		},
		"resource_access": {
			"qhub_qadmin": {
				"roles": ["administrator"]
			},
			"jakarta_school": {
				"roles": ["user"]
			},
			"account": {
				"roles": ["manage-account", "manage-account-links", "view-profile"]
			}
		},
		"scope": "email profile",
		"sid": "4ce6f125-227d-4202-935b-e392ba061ebb",
		"email_verified": True,
		"name": "qadmin admin user",
		"preferred_username": "qadmin_admin",
		"given_name": "qadmin",
		"family_name": "admin user",
		"email": "admin_qadmin@somewhere.com"
	}

@pytest.fixture
def role_user_payload(default_payload):
	payload_json["resource_access"][DEFAULT_CONTEXT]["roles"] = ["user"]

@pytest.fixture
def role_project_mgr_payload(default_payload):
	payload_json["resource_access"][DEFAULT_CONTEXT]["roles"] = ["project_manager"]

# This modifies the payload 
def test_admin_payload(default_payload):
	# get administrator policy from payload
	policy = get_qadmin_policy_for_user(payload_json)
	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), DEFAULT_CONTEXT)
	print(policy)
	print(f"administrator: allow project_overview? {allow}")
	assert allow == True
	print()

def test_user_paylaod(role_user_payload):
	# get user policy from paylaod
	policy = get_qadmin_policy_for_user(payload_json)
	print(policy)
	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), DEFAULT_CONTEXT)
	print(f"user: allow project_overview? {allow}")
	assert allow == False

	allow = policy.should_allow(Permission("http.GET",RN_EMPLOYEE), DEFAULT_CONTEXT)
	print(f"user: allow employee? {allow}")
	assert allow == True
	print()

def test_pm_payload(role_project_mgr_payload):
	# fix json to have roles user only.
	policy = get_qadmin_policy_for_user(payload_json)
	print(policy)
	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), DEFAULT_CONTEXT)
	print(f"project_manager: allow project_overview? {allow}")
	assert allow == True
	print()


# test context
def test_policy_context_change(default_payload):
	# default administrator
	policy = get_qadmin_policy_for_user(payload_json)
	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), DEFAULT_CONTEXT)
	print(policy)
	print(f"admin: context=qhub_qadmin role=administrator - allow project_overview? {allow}")
	assert allow == True

	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), "jakarta_school")
	print(f"admin: context=jakarta_school role=user - allow project_overview? {allow}")
	assert allow == False

	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), "unknown")
	print(f"admin: context=unknown no role - allow project_overview? {allow}")
	assert allow == False
	print()


# test policy load unload
def test_policy_export_load(default_payload):
	# default administrator
	policy = get_qadmin_policy_for_user(payload_json)
	allow = policy.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), "jakarta_school")
	print("POLICY: {}".format(policy))

	policy_json = policy.to_json()
	policy_str = json.dumps(policy_json)

	policy2 = get_policy(policy_str)
	print("POLICY 2: {}".format(policy2))
	allow2 = policy2.should_allow(Permission("http.GET",RN_PROJECT_OVERVIEW), "jakarta_school")

	assert allow == allow2
	assert policy_json == policy2.to_json()
