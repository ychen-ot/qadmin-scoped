# Use keycloak api to get tokens. 
import keycloak
# from keycloak import KeycloakOpenID

class KeycloakClient():
    def __init__(self, server_url, realm_name, client_id, client_secret_key):

        self.client = keycloak.KeycloakOpenID(
            server_url=server_url,
            realm_name=realm_name,
            client_id=client_id,
            client_secret_key=client_secret_key)
        self.public_key = "-----BEGIN PUBLIC KEY-----\n" + self.client.public_key() + "\n-----END PUBLIC KEY-----"

    # This also logs in. 
    def get_user_token(self, uid: str, passwd: str):
        token = self.client.token(uid, passwd)
        return token

    # Use access_token for decoding
    # This will throw exceptions for permission issues.
    def decode_and_verify_token(self, token):
        # This verify the token
        token_str = self._get_token_str(token, 'access_token')
        if not token_str:
            return None 

        # This verify the token
        # TODO: - figure out how to configure so verify_aud can be set to True
        options = {"verify_signature": True, "verify_aud": False, "verify_exp": True}
        token_info = self.client.decode_token(token_str, key=self.public_key, options=options)
        return token_info


    # Use access_token to retrieve user_info
    def get_user_info(self, token):
        # This verify the token
        token_str = self._get_token_str(token, 'access_token')
        if not token_str:
            return None
        return self.client.userinfo(token_str)


    # Use refresh_token to logout
    def logout(self, token):
        # refresh token is used to log a client out.
        token_str = self._get_token_str(token, 'refresh_token')
        # this will likely throw an exception
        self.client.logout(token_str)


    def _get_token_str(self, token, field):
        if isinstance(token, str):
            return token
        if field in token: 
            return token[field]
        return None
