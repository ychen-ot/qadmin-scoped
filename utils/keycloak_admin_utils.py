# Use keycloak api to work with admin endpoints
import keycloak


# QUESTION: do we want to do anything w/ Realm Roles? 
# Need to think about how to return error consistently
class KeycloakAdminClient():
    # I think admin_user / admin_pwd can be substituted with client_id / client_secret ...
    def __init__(self, server_url, admin_user, admin_pwd, admin_realm, realm_name):
        self.admin_client = keycloak.KeycloakAdmin(
            server_url=server_url,
            username=admin_user,
            password=admin_pwd,
            user_realm_name=admin_realm,
            realm_name=realm_name,
            auto_refresh_token=("get", "put", "post", "delete"))

    # Get users
    def get_users(self):
        return self.admin_client.get_users({})

    # Get client_roles. [ client_role, ... ]
    # Example: output (can be None)
    # {'id': '4e04b8f0-ed82-47e2-96c8-6aaa1ff5b22c', 'name': 'administrator', 'description': 'Administrator to the system', 
    #        'composite': False, 'clientRole': True, 'containerId': '29977088-42b5-4fe9-afed-7884c07376dd'}
    def get_client_roles(self, client_name):
        client_id = self.admin_client.get_client_id(client_name)
        if not client_id:
            return None
        return self.admin_client.get_client_roles(client_id=client_id)

    def get_client_role(self, client_name, role_name):
        client_id = self.admin_client.get_client_id(client_name)
        if not client_id:
            return None
        try:
            role = self.admin_client.get_client_role(client_id, role_name)
            return role
        except Exception as e:
            print(e)
        return None

    
    # only composite roles.  These applies per client.  
    # Client name is used to construct the jwt_json payload, where the client_id is used for queries
    def get_user_roles_for_client(self, user_name, client_name):
        user_id = self.admin_client.get_user_id(user_name)
        client_id = self.admin_client.get_client_id(client_name)
        if not client_id or not user_id:
            # should do a warning.
            print(f"Id not found for user: {user_name}:{user_id} for client: {client_name}:{client_id} ")
            return None
        try:
            roles_json = self.admin_client.get_composite_client_roles_of_user(user_id=user_id, client_id=client_id)
            jwt_json = self._extract_roles_from_composite(roles_json, client_name)
            return jwt_json

        except Exception as e:
            print(e)
        return None


    # Transform result from keycloak - Example for a composite role. 
    # [{'id': '..', 'name': 'people_manager', 'description': '..', 'composite': False, 'clientRole': True, 'containerId': '..'}, 
    #  {'id': '..', 'name': 'project_manager', 'description': '..', 'composite': False, 'clientRole': True, 'containerId': '..'},
    #  {'id': '..', 'name': 'project_and_people_mgr', 'description': ..', 'composite': True, 'clientRole': True, 'containerId': '..'}]
    # Output to something similar to jwt payload ("resources_access" seection)
    #  {"resource_access": { "qhub_qadmin": { "roles": [ "people_manager", "project_manager" ]}}}
    def _extract_roles_from_composite(self, role_json, client_name):
        client_roles = []
        for item in role_json: 
            if item["clientRole"] and not item["composite"]: 
                client_roles.append(item["name"])
        # construct the json:
        res = { "resource_access": {
            client_name: {
                "roles": client_roles
                }
            }
        }
        return res


    # Creates client role
    # NOTE: return is blank if no issues
    #  otherwise - return {'Already exists'}
    def create_client_role(self, client_name, role_name, role_description):
        client_id = self.admin_client.get_client_id(client_name)
        if not client_id:
            return {'Invalid client-name provided'}
        
        client_role = self.admin_client.create_client_role(
            client_role_id=client_id, 
            payload={'name': role_name, 'description': role_description, 'clientRole': True},
            skip_exists=True)

        return client_role

    
    def delete_client_role(self, client_name, role_name):
        try:
            client_id = self.admin_client.get_client_id(client_name)
            if not client_id:
                return {'Invalid client-name provided'}
            return self.admin_client.delete_client_role(client_role_id=client_id, role_name=role_name)
        except Exception as e:
            print(e)
        return None

    # Client update may be 
    # http://localhost:8081/auth/admin/realms/education/roles-by-id/2574210d-0a17-4df6-9a3c-e7ce65cf885f
    # PUT - payload / with appropriate headers.
    # {"id":"2574210d-0a17-4df6-9a3c-e7ce65cf885f","name":"people_manager",
    #  "description":"Manages people that belongs to the organization","composite":false,
    #  "clientRole":true,"containerId":"29977088-42b5-4fe9-afed-7884c07376dd","attributes":{}}
    # 
    # There is no function that is available thru the administrative api. 
    # To update, you'll need to get role by id and use a PUT comment.
    def update_client_role(self, role_id, role_description):
        print("not implemented... ")

