
from scoped_rbac.policy import RootPolicy, Permission, policy_from_json
from config import DEFAULT_CONTEXT
import json

URN_NID_NSS="urn:ot:qadmin"
RN_EMPLOYEE=f"{URN_NID_NSS}/employee"
RN_PROJECT_OVERVIEW=f"{URN_NID_NSS}/project_overview_all"
RN_EMPLOYEE_UTIL=f"{URN_NID_NSS}/employee_availability"
RN_EMPLOYEE_AVIL=f"{URN_NID_NSS}/employee_utilization"
RN_EMPLOYEE_LOG=f"{URN_NID_NSS}/logs"
EMPLOYEE_PM_ALL="employee_all"  # or urn

###
# How to represent that user can only see part of the employee_panel ?
# urn - urn:<name_space_identifier>:<name_space_specific_string>#fragment
# where does rbac_context go? would we want the roles to be `quansight:administrator`
QADMIN_ROLES = {
    "admin": True,
    "project_manager": {
        "http.GET": [
            RN_EMPLOYEE,
            RN_PROJECT_OVERVIEW,
            RN_EMPLOYEE_AVIL,
        ],
        EMPLOYEE_PM_ALL: True
    },
    "people_manager": {
        "http.GET": [
            RN_EMPLOYEE,
            RN_EMPLOYEE_UTIL,
            RN_EMPLOYEE_AVIL,
        ],
        EMPLOYEE_PM_ALL: True
    },
    "employee": {
        "http.GET": [
            RN_EMPLOYEE,
        ],
        EMPLOYEE_PM_ALL: False
    },
    "sysadmin": {
        "http.GET": [
            RN_EMPLOYEE_LOG,
        ]
    }
}


# get policy base on user roles given context.
# Token payload json is used here.  Example:
# payload: { "resource_access": { "qhub_qadmin": { "roles": ["administrator"]},
#                                 "jakarta-school": { "roles": ["create-student-grade"]},
#               ... }]
# Concern is that - the client might define another "administrator" that has a different meaning?
# 
# or do we want to extract context from email? 
def get_qadmin_policy_for_user(payload):
    policy = RootPolicy()
    resource_acc = payload["resource_access"] if payload and "resource_access" in payload else None
    if not resource_acc:
        print(f"ERROR: Role information is not available.")
        return policy
    
    for context, roles in resource_acc.items():
        # add policy for each role defined in the context. 
        roles_list = roles["roles"] if "roles" in roles else None
        for role in roles_list:
            if role in QADMIN_ROLES:
                print(f"adding: role {role} definition: {QADMIN_ROLES[role]}, context: {context}")
                policy.add_json_policy_for_context(QADMIN_ROLES[role], context)
    return policy


# wrapper method.
def should_allow(policy, action, resource=None, context=DEFAULT_CONTEXT):
    return policy.should_allow(Permission(action=action, resource_type=resource), context)

# serializing policy
def get_policy_as_json(policy):
   return policy.to_json()

def get_policy(json_policy):
    if isinstance(json_policy, str): 
        print("is json")
        json_policy = json.loads(json_policy)
    
    policy = RootPolicy()
    policy.add_policy(policy_from_json(json_policy))
    return policy
