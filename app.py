import os
import functools
import json
import panel as pn
from dotenv import load_dotenv
load_dotenv()

from utils.keycloak_utils import KeycloakClient
import config

from permissions import (
    get_qadmin_policy_for_user,
    should_allow,
    RN_EMPLOYEE,
    RN_EMPLOYEE_AVIL,
    RN_EMPLOYEE_UTIL,
    RN_PROJECT_OVERVIEW,
    EMPLOYEE_PM_ALL,
)

# Keycloak configuration 
client = KeycloakClient(
        server_url=config.KEYCLOAK_SERVER,
        realm_name=config.KEYCLOAK_REALM,
        client_id=config.KEYCLOAK_CLIENT_NAME,
        client_secret_key=config.KEYCLOAK_CLIENT_SECRET,
    )

# better ways to require a specific resource inside the object decorated
def check_access(action, resource=None, no_access_obj=None):
    def decorator_check_access(func):
        @functools.wraps(func)
        def wrapper_check_access(self, *args, **kwargs):
            print(f"Input: action: {action} resource: {resource}")
            user_policy = self.user_policy
            has_access = should_allow(user_policy, action, resource)
            if has_access:
                # call the function and return it.
                value = func(self, *args, **kwargs)
                return value
            # if no access - return object specified in `no_access_obj`
            return no_access_obj 
        return wrapper_check_access
    return decorator_check_access


# decorator requires that user_policy or get_user_policy method is defined.
class App:
    def __init__(self):
        self.setup_user_policy()
        
        # four buttons representing the various panel applications.
        self.button_composite = self.get_employee()
        self.button_2 = self.get_project_overview()
        self.button_3 = self.get_employee_availability()
        self.button_4 = self.get_employee_utilization()

        # layout
        self.layout = pn.Column(
            pn.Row(self.button_composite, self.button_2, self.button_3, self.button_4)
        )
        self.layout.servable()

    def setup_user_policy(self):
        # This is always a test?
        user = config.TEST_UID
        passwd = config.TEST_PASSWORD
        print(f"USER: {user}, PASSWORD: {passwd}")
        
        self.token = client.get_user_token(user, passwd)
        self.payload_json = client.decode_and_verify_token(self.token)
        self.user_policy = get_qadmin_policy_for_user(self.payload_json)


    def click_update(self, event):
        headers = pn.state.headers
        print("headers: {}".format(headers))
        print("HOST: {}".format(headers['Host']))
        print("payload_data: {}".format(json.dumps(self.payload_json, indent=2)))


    @check_access(action="http.GET", resource=RN_EMPLOYEE)
    def get_employee(self):
        button = pn.widgets.Button(name="Employee", button_type="primary", width=150)
        button.on_click(self.click_update)

        # dropdown list
        user_list = ["current_user"]
        if should_allow(self.user_policy, EMPLOYEE_PM_ALL):
            user_list = ["current_user", "user1", "user2", "user3", "user4"]       

        selection = pn.widgets.Select(options=user_list, width=150)
        object_item = pn.Column(
            button, selection
        )
        return object_item


    @check_access(action="http.GET", resource=RN_PROJECT_OVERVIEW)
    def get_project_overview(self):
        button = pn.widgets.Button(name="Project Overview", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button


    @check_access(action="http.GET", resource=RN_EMPLOYEE_AVIL)
    def get_employee_availability(self):
        button = pn.widgets.Button(name="Employee Availability", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button


    @check_access(action="http.GET", resource=RN_EMPLOYEE_UTIL)
    def get_employee_utilization(self):
        button = pn.widgets.Button(name="Employee Utilization", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button

App().layout
