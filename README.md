# Qadmin - scoped - rbac example.

Sample code demonstrating using scoped-rbac for permissions to qadmin pages.
- Code retrieves a user token from keycloak server
- The token is decoded, and processed to determine whether UI components should be displayed or not.

## Getting started

### Keycloak configuration

1. Run a keycloak container instance.  I am using a slightly older version of keycloak (16.1.1)
```
docker run -p 8081:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak
```

Follow instructions: https://gitlab.com/openteams/automation-and-integration/backlog/-/issues/522#note_876113283
  - This includes instructions on:
     - Client configuration
     - Role configuration
     - Group configuration
     - User configuration.  You'll need the user id and user password to test the code below.


### Running the sample app.

To run locally, do:
1. Setup conda environment
```
conda env create -f environment.yml
```

2. Activate the environment
```
conda activate qadmin_scope
```

3. Set up `.env` file. Fill in UID and PASSWORD for the user you are testing.
```
cp .env.tpl .env
vi .env
```

4. Run the application

`app.py` :  Tests retrieving an user token from TEST_ID, and TEST_PASSWORD and creating a policy with it.
```
# Test using an userid_password to retrieve payload token
panel serve app.py --use-xheaders
```

`app_qhub.py` :  Tests retrieving user_name from X-Forwarded-User, and retrieve role from keycloak. 
```
# Test using `X-Forwarded-User` to store user_name 
panel serve app_qhub.py --use-xheaders
```

NOTE: To run as how the docker container is launched
```
vi .env
source .env
bokeh serve app_qhub.py --port=80 --use-xheaders --allow-websocket-origin=$DEPLOY_HOST --prefix=$PREFIX
```


5. Browse to the URL as indicated by the bokeh server: `http://localhost:5006/<PREFIX>/app_qhub`

6. To run test:
```
conda activate qadmin_scope
export PYTHONPATH=.:$PYTHONPATH
pytest tests/

```


### Deployment

1. Build dockerfile. Change version if needed (sometimes a new version is needed to redeploy)
```
docker build . -t registry.gitlab.com/ychen-ot/qadmin-scoped/qhub_qadmin_scope:v0.4

```

2. To test locally, you will want to provide environment information.  These are values to configure fo the docker runtime, and the same set of environments would need to be available as part of the deployment described below
```
docker run -d -p 80:80 \
 -e KEYCLOAK_SERVER="https://qhub.openteams.com/auth/realm" \
 -e KEYCLOAK_REALM="qhub" \
 -e KEYCLOAK_CLIENT_NAME="qhub_qadmin" \
 -e KEYCLOAK_ROLE_ADM="role_reader" \
 -e KEYCLOAK_ROLE_ADM_PASSWD='role_reader_password' \
 -e TEST_UID='USE_XFORWARD_HEADER' \
 -e DEPLOY_HOST='localhost' \
 -e PREFIX='hello' \
registry.gitlab.com/ychen-ot/qadmin-scoped/qhub_qadmin_scope:v0.6
```
  - Once the container is running, use `docker logs -f <container_id>` to determine the host path.
  - Use: `docker ps` to find the container, and use `docker stop` and `docker rm` to remove the container when done testing.
  - `TEST_UID` is an user_name value you can use to test the application.  This needs to be an user in the realm.
  - If `TEST_UID` = `USE_XFORWARD_HEADER`, then `X-Forwarded-User` in the header will be used.
  - `DEPLOY_HOST` - is the value provided for `--allow-websocket-origin=$DEPLOY_HOST` during bokeh server launch
  - `PREFIX` - is the value provided for  `--prefix=$PREFIX` during bokeh server launch


3. Follow this instruction to upload the image to the repo. https://docs.google.com/document/d/1BxsPMjrUGosFUWjJLb5pGck6uVSrjur7hXmIsdtrSsQ
```
export CI_USER="<user>"
export CI_PWD="<token>"
docker login -u $CI_USER -p $CI_PWD https://registry.gitlab.com/ychen-ot/qadmin-scoped/
docker push registry.gitlab.com/ychen-ot/qadmin-scoped/qhub_qadmin_scope:v0.4
```

4. Fill in missing environments for the deployment. Recommend to create a copy of deployment.yml
```
cp deployment.yml deployment_env.yml
```

5. run
```
kubectl apply -f deployment_env.yml
```

6. Check to see if the deployment is running:
```
kubectl -n dev get pod | grep scope  
kubectl -n dev logs -f <pod_name>
Ctrl-C to exit
```

7. Check the app. The following SHOULD be the URL.  NOTE: this is currently note quite working...
```
https://qhub.openteams.com/qhub-qadmin-scope/app_qhub
```

8. To redeploy without deleting the deploymet. Update the `deployment.yml` to trigger the code to re-run...

9. To clean up
```
kubectl -n dev delete IngressRoute qhub-qadmin-scope
kubectl -n dev delete service qhub-qadmin-scope 
kubectl -n dev delete deploy qhub-qadmin-scope
```


