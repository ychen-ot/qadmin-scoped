import os
import functools
import json
import panel as pn
from dotenv import load_dotenv
load_dotenv()

from utils.keycloak_admin_utils import KeycloakAdminClient
import config

from permissions import (
    get_qadmin_policy_for_user,
    should_allow,
    RN_EMPLOYEE,
    RN_EMPLOYEE_AVIL,
    RN_EMPLOYEE_UTIL,
    RN_PROJECT_OVERVIEW,
    EMPLOYEE_PM_ALL,
)

USE_XFORWARD_HEADER="USE_XFORWARD_HEADER"

# Keycloak configuration 
admin_client = KeycloakAdminClient(
    server_url=config.KEYCLOAK_SERVER,
    admin_user=config.KEYCLOAK_ROLE_ADM,
    admin_pwd=config.KEYCLOAK_ROLE_ADM_PASSWD,
    admin_realm=config.KEYCLOAK_REALM,
    realm_name=config.KEYCLOAK_REALM,
)

# better ways to require a specific resource inside the object decorated
def check_access(action, resource=None, no_access_obj=None):
    def decorator_check_access(func):
        @functools.wraps(func)
        def wrapper_check_access(self, *args, **kwargs):
            print(f"Input: action: {action} resource: {resource}")
            user_policy = self.user_policy
            has_access = should_allow(user_policy, action, resource)
            print(f"has_access: {has_access}")
            if has_access:
                # call the function and return it.
                value = func(self, *args, **kwargs)
                return value
            # if no access - return object specified in `no_access_obj`
            return no_access_obj 
        return wrapper_check_access
    return decorator_check_access


# decorator requires that user_policy or get_user_policy method is defined.
class App:
    def __init__(self):
        self.setup_user_policy()
        
        # A button to view everything.
        self.button_all = pn.widgets.Button(name="ALL Users", button_type="primary", width=150)
        self.button_all.on_click(self.click_update)

        # four buttons representing the various panel applications.
        self.button_composite = self.get_employee()
        self.button_2 = self.get_project_overview()
        self.button_3 = self.get_employee_availability()
        self.button_4 = self.get_employee_utilization()

        # layout
        self.layout = pn.Column(
            pn.Row(self.button_all, self.button_composite, self.button_2, self.button_3, self.button_4)
        )
        self.layout.servable()

    # Retrieve user information from panel application.
    def setup_user_policy(self):
        self.user_name = self._get_user()
        self.user_roles = self._get_user_roles(self.user_name)
        self.user_policy = get_qadmin_policy_for_user(self.user_roles)
        print(f"-----POLICY: {self.user_policy} ------- ")

    def _get_user(self):
        # mock
        if config.TEST_UID and config.TEST_UID != USE_XFORWARD_HEADER: 
            pn.state.headers['X-Forwarded-User'] = config.TEST_UID

        headers = pn.state.headers
        user_name = headers.get('X-Forwarded-User') if "X-Forwarded-User" in headers else None
        if not user_name:
            print(f"Unable to determine user.  Header: X-Forwarded-User not available")
        return user_name

    def _get_user_roles(self, user_name):
        if user_name: 
            return admin_client.get_user_roles_for_client(user_name, config.KEYCLOAK_CLIENT_NAME)
        return None

    def click_update(self, event):
        headers = pn.state.headers
        print("headers: {}".format(headers))
        print(f"User: {self.user_name}, Policy: {self.user_policy}, Role: {self.user_roles}")
        print(f"Configs: KEYCLOAK_CLIENT_NAME: {config.KEYCLOAK_CLIENT_NAME} , KEYCLOAK_SERVER: {config.KEYCLOAK_SERVER}")


    @check_access(action="http.GET", resource=RN_EMPLOYEE)
    def get_employee(self):
        button = pn.widgets.Button(name="Employee", button_type="primary", width=150)
        button.on_click(self.click_update)

        # dropdown list
        user_list = ["current_user"]
        if should_allow(self.user_policy, EMPLOYEE_PM_ALL):
            user_list = ["current_user", "user1", "user2", "user3", "user4"]       

        selection = pn.widgets.Select(options=user_list, width=150)
        object_item = pn.Column(
            button, selection
        )
        return object_item


    @check_access(action="http.GET", resource=RN_PROJECT_OVERVIEW)
    def get_project_overview(self):
        button = pn.widgets.Button(name="Project Overview", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button


    @check_access(action="http.GET", resource=RN_EMPLOYEE_AVIL)
    def get_employee_availability(self):
        button = pn.widgets.Button(name="Employee Availability", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button


    @check_access(action="http.GET", resource=RN_EMPLOYEE_UTIL)
    def get_employee_utilization(self):
        button = pn.widgets.Button(name="Employee Utilization", button_type="primary", width=150)
        button.on_click(self.click_update)
        return button

App().layout
