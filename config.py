import os

# To retrieve values from environmnet. 

# Client that contains role definitions.
KEYCLOAK_SERVER=os.getenv('KEYCLOAK_SERVER', None)
KEYCLOAK_REALM=os.getenv('KEYCLOAK_REALM', None)
KEYCLOAK_CLIENT_NAME=os.getenv('KEYCLOAK_CLIENT_NAME', None)
KEYCLOAK_CLIENT_SECRET=os.getenv('KEYCLOAK_CLIENT_SECRET', None)

# User that interfaces with the keycloak administrator
KEYCLOAK_ROLE_ADM=os.getenv('KEYCLOAK_ROLE_ADM', None)
KEYCLOAK_ROLE_ADM_PASSWD=os.getenv('KEYCLOAK_ROLE_ADM_PASSWD', None)

# User for testing application
# NOTE: set TEST_UID = "" - to allow actual header to be used in app_qhub.py
TEST_UID = os.getenv("TEST_UID","user")
TEST_PASSWORD = os.getenv("TEST_PASSWORD", None)

DEFAULT_CONTEXT = KEYCLOAK_CLIENT_NAME
